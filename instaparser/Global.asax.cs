﻿using instaparser.AdditionalMethods;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace instaparser
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected async void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            await Launch.LoginAsync();
            Launch.InitializeComponents();
        }
    }
}
