﻿using InstaSharper.API;
using InstaSharper.API.Builder;
using InstaSharper.Classes;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace instaparser.AdditionalMethods
{
    public class Launch : Controller
    {
        public static IInstaApi api;
        public static UserSessionData user;
        public static List<string> actionWithUser;
        public static List<string> actionWithPost;

        public static async Task<bool> LoginAsync()
        {
            user = new UserSessionData { UserName = "instaparser123", Password = "instaparser515" };
            api = InstaApiBuilder.CreateBuilder()
                .SetUser(user)
                .Build();

            var log = await api.LoginAsync();
            return log.Succeeded;
        }

        public static void InitializeComponents()
        {
            actionWithUser = new List<string>() { "Подписчики", "Подписки" };
            actionWithPost = new List<string>() { "Лайки", "Комменты" };
        }
    }
}