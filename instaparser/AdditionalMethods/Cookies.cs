﻿
using System;
using System.Collections.Generic;
using System.Web;

namespace instaparser.AdditionalMethods
{
    public static class Cookies
    {
        public static HttpCookie AddCookie(dynamic name, dynamic value)
        {

            return new HttpCookie(name, value)
            {
                Expires = DateTime.Now.AddDays(1)
            };
        }
    }
}