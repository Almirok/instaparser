﻿using instaparser.Models;
using InstaSharper.API;
using InstaSharper.Classes;
using InstaSharper.Classes.Models;
using Ionic.Zip;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace instaparser.AdditionalMethods
{
    public class WorkWithUsername : Controller
    {
        public string FilePath { get; set; }
        public string FileName { get; set; }

        public async Task<ActionResult> GetFollowers(InstaModel model, IInstaApi api)
        {
            IResult<InstaUserShortList> followers = null;
            string[] stringSeparators = new string[] { "\r\n" };
            var usernames = model.Username?.Split(stringSeparators, StringSplitOptions.None);
            if (usernames != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    using (TextWriter tw = new StreamWriter(ms))
                    {
                        for (int i = 0; i < usernames.Length; i++)
                        {
                            followers = await api.GetUserFollowersAsync(usernames[i], PaginationParameters.MaxPagesToLoad(model.Count / 199 + 1));
                            if (followers.Succeeded)
                            {
                                model.Count = model.Count > followers.Value.Count ? followers.Value.Count : model.Count;

                                if (model.WordsForFilter != null)
                                {
                                    await FilterUsers(api, model, followers, model.Count, tw);
                                }
                                else
                                {
                                    for (int j = 0; j < model.Count; j++)
                                    {
                                        await tw.WriteLineAsync(followers.Value[j].UserName);
                                    }
                                }
                            }
                            else { return null; }
                        }
                    }
                    return File(ms.GetBuffer(), "text/plain", model.Username + new Random().Next(0, int.MaxValue) + ".txt");
                }
            }
            else { return View(); }
        }

        public static async Task FilterUsers(IInstaApi api, InstaModel model, IResult<InstaUserShortList> users, int count, TextWriter tw)
        {
            Task[] tasks = new Task[2];
            string[] words = model.WordsForFilter?.Split(',');
            count = count > users.Value.Count ? users.Value.Count : count;

            tasks[0] = Task.Run(async () =>
            {
                for (int i = 0; i < count / 2; i++)
                {
                    bool isFind1 = false;
                    IResult<InstaUserInfo> bio1 = null;
                    bio1 = await api.GetUserInfoByUsernameAsync(users.Value[i].UserName);
                    if (bio1.Succeeded)
                    {
                        foreach (var item in words)
                        {
                            if (bio1.Value.Biography.ToLower().Contains(item.ToLower().Trim()))
                            {
                                isFind1 = true;
                                break;
                            }
                        }
                    }
                    if (!isFind1)
                    {
                        await tw.WriteLineAsync(users.Value[i].UserName);
                    }
                }
            });
            tasks[1] = Task.Run(async () =>
            {
                for (int j = count / 2; j < count; j++)
                {
                    bool isFind2 = false;
                    IResult<InstaUserInfo> bio2 = null;
                    bio2 = await api.GetUserInfoByUsernameAsync(users.Value[j].UserName);
                    if (bio2.Succeeded)
                    {
                        foreach (var item in words)
                        {
                            if (bio2.Value.Biography.ToLower().Contains(item.ToLower().Trim()))
                            {
                                isFind2 = true;
                                break;
                            }
                        }
                    }
                    if (!isFind2)
                    {
                        await tw.WriteLineAsync(users.Value[j].UserName);
                    }
                }
            });
            Task.WaitAll(tasks);
        }

        public async Task<FilePathResult> GetImages(InstaModel model, IInstaApi api)
        {
            var images = await api.GetUserMediaAsync(model.Username, PaginationParameters.MaxPagesToLoad(model.Count / 12 + 1));

            var path = AppDomain.CurrentDomain.BaseDirectory + "Images\\";
            Directory.CreateDirectory(path);
            if (!images.Succeeded)
            {
                return null;
            }
            else
            {
                if (model.Count > images.Value.Count) model.Count = images.Value.Count;

                using (WebClient myWebClient = new WebClient())
                {
                    using (var zip = new ZipFile())
                    {
                        for (int i = 0; i < model.Count; i++)
                        {
                            if (images.Value[i].Carousel != null)
                            {
                                for (int j = 0; j < images.Value[i].Carousel.Count; j++)
                                {
                                    myWebClient.DownloadFile(images.Value[i].Carousel[j].Images[0].URI, path + model.Username + new Random().Next(0, int.MaxValue) + ".jpeg");
                                }

                            }
                            else
                            {
                                myWebClient.DownloadFile(images.Value[i].Images[0].URI, path + model.Username + new Random().Next(0, int.MaxValue) + ".jpeg");
                            }
                        }
                        string packageName = model.Username + new Random().Next(0, int.MaxValue) + ".zip";
                        zip.AddDirectory(path);
                        zip.Save(AppDomain.CurrentDomain.BaseDirectory + packageName);
                        var file = File(AppDomain.CurrentDomain.BaseDirectory + packageName, "application/zip", packageName);
                        Directory.Delete(path, true);
                        FilePath = file.FileName;
                        return file;
                    }
                }
            }
        }

    }
}