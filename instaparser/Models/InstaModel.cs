﻿
using System;
using System.Web;

namespace instaparser.Models
{
    public class InstaModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public int Count { get; set; }
        public string Action { get; set; }
        public string UrlToPost { get; set; }
        public HttpPostedFileBase File { get; set; }
        public string WordsForFilter { get; set; } 
        public string Species { get; set; }
        public DateTime Date1 { get; set; }
        public DateTime Date2 { get; set; }
    }
}