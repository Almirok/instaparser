﻿using instaparser.AdditionalMethods;
using instaparser.Models;
using InstaSharper.API;
using InstaSharper.API.Builder;
using InstaSharper.Classes;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace instaparser.Controllers
{
    public class InstaController : Controller
    {
        static WorkWithUsername pars = new WorkWithUsername();
        public static bool succeed;

        public async Task<ActionResult> Index()
        {
            try
            {
                if (Request.Cookies["username"].Value != null && Launch.user.UserName != Request.Cookies["username"].Value)
                {
                    Launch.user = new UserSessionData()
                    {
                        UserName = Request.Cookies["username"].Value,
                        Password = Request.Cookies["password"].Value
                    };

                    Launch.api = InstaApiBuilder.CreateBuilder().SetUser(Launch.user).SetRequestDelay(RequestDelay.FromSeconds(1, 2)).Build();

                    var log = await Launch.api.LoginAsync();
                }
            }
            catch (Exception)
            {

            }


            return View();
        }

        public ActionResult Work()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> WorkWithUser(InstaModel model)
        {
            var file = await pars.GetFollowers(model, Launch.api);

            ViewBag.Actions = new SelectList(Launch.actionWithUser);
            if (file == null)
            {
                ModelState.AddModelError(string.Empty, "Введене неверный имя пользователя или пароль");
                return View("Work", model);
            }
            return file;

        }

        [HttpPost]
        public async Task<ActionResult> GetImages(InstaModel model)
        {
            var file = await pars.GetImages(model, Launch.api);

            ViewBag.Actions = new SelectList(Launch.actionWithUser);
            if (file == null)
            {
                ModelState.AddModelError(string.Empty, "Введено неверное имя пользователя или пароль");
                return View("Work", model);
            }
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + file.FileDownloadName + ";");
            Response.TransmitFile(pars.FilePath);
            Response.Flush();
            System.IO.File.Delete(pars.FilePath);
            return Work();
        }

        public async Task<ActionResult> Authorize(InstaModel model)
        {
            Launch.user = new UserSessionData { UserName = model.Username, Password = model.Password };
            Launch.api = InstaApiBuilder.CreateBuilder()
                .SetUser(Launch.user)
                .SetRequestDelay(RequestDelay.FromSeconds(1, 2))
                .Build();

            var log = await Launch.api.LoginAsync();

            if (log.Succeeded)
            {
                Response.SetCookie(Cookies.AddCookie("username", model.Username));
                return RedirectToAction("Index", "Insta");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Вы ввели неверное имя пользователя или пароль");
                return View("Index", model);
            }
        }
    }
}